# SiteMapper

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/jackhughes/sitemapper)](https://goreportcard.com/report/gitlab.com/jackhughes/sitemapper)

Basic command line Go application that will take a base URL and provide a list of linked pages in JSON format. The JSON file produced at the end is in the directory the application was run from, and is named `sitemap.json`

## Prerequisites
-   Go version 1.11.2 upwards, previous versions will likely still work.
-   Install all appropriate packages with `go get ./...` from the root directory of the repository.

## Running the app
-   `go run main.go crawler.go` without a URL tag will default to the URL https://monzo.com.
-   By passing the `--url` flag, a user can determine a different base path to crawl. 
-   The output of the application is found in the working dir named `sitemap.json`.

## Running the tests
-   Included in this repository are unit, integration and benchmarking tests.
-   To run the unit and integration tests, simply run `go test`
-   In order to generate a HTML coverage report, run `go test ./... -race -cover -coverprofile=coverage.out` and then `go tool cover -html=coverage.out`
-   In order to run the benchmark test, run: `go test -bench=.`