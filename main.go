package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"

	"github.com/sirupsen/logrus"
)

func main() {
	u := flag.String("url", "https://monzo.com/", "a url to generate a sitemap for")
	flag.Parse()

	c := New(*u)
	c.crawl(*u)

	j, err := json.MarshalIndent(c, "", "    ")
	if err != nil {
		logrus.Errorf("failed to unmarshal json: %v", err)
	}

	if err := ioutil.WriteFile("sitemap.json", j, 0644); err != nil {
		logrus.Errorf("failed to write file: %v", err)
	}
}
