package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"
)

const base string = "https://monzo.com"

var responseCodes = []struct {
	name string
	in   *http.Response
	err  bool
}{
	{
		name: "Success 200 HTML",
		in: &http.Response{
			StatusCode: http.StatusOK,
			Header:     map[string][]string{"Content-Type": []string{"text/html"}}},
	},
	{
		name: "Failure 307 HTML",
		in: &http.Response{
			StatusCode: http.StatusTemporaryRedirect,
			Header:     map[string][]string{"Content-Type": []string{"text/html"}}},
		err: true,
	},
	{
		name: "Failure 502 HTML",
		in: &http.Response{
			StatusCode: http.StatusBadRequest,
			Header:     map[string][]string{"Content-Type": []string{"text/html"}}},
		err: true,
	},
	{
		name: "Failure 500 HTML",
		in: &http.Response{
			StatusCode: http.StatusInternalServerError,
			Header:     map[string][]string{"Content-Type": []string{"text/html"}}},
		err: true,
	},
	{
		name: "Failure 200 JSON",
		in: &http.Response{
			StatusCode: http.StatusOK,
			Header:     map[string][]string{"Content-Type": []string{"application/json"}}},
		err: true,
	},
	{
		name: "Failure 200 XML",
		in: &http.Response{
			StatusCode: http.StatusOK,
			Header:     map[string][]string{"Content-Type": []string{"application/xml"}}},
		err: true,
	},
	{
		name: "Failure 200 JPEG",
		in: &http.Response{
			StatusCode: http.StatusOK,
			Header:     map[string][]string{"Content-Type": []string{"image/jpeg"}}},
		err: true,
	},
}

var linksToProcess = []struct {
	name string
	in   string
	out  string
}{
	{
		name: "Successful link",
		in:   "/home",
		out:  base + "/home",
	},
	{
		name: "Successful link with 2 paths",
		in:   "/home/12",
		out:  base + "/home/12",
	},
}

var fetched = []struct {
	name string
	body io.ReadCloser
	url  string
	out  *Page
}{
	{
		name: "<a> tags present",
		body: ioutil.NopCloser(strings.NewReader("<a href=\"/hello/world\">Test Content</a><a href=\"/world/hello\">Test Content 2</a>")),
		url:  base,
		out: &Page{
			URL:   base,
			Links: []string{"/hello/world", "/world/hello"},
		},
	},
	{
		name: "<a> tags present",
		body: ioutil.NopCloser(strings.NewReader("<a href=\"/1/2/3/4/5/6/7/8/9\">Test Content</a><a href=\"/9/8/7/6/5/4/3/2/1\">Test Content 2</a>")),
		url:  base,
		out: &Page{
			URL:   base,
			Links: []string{"/1/2/3/4/5/6/7/8/9", "/9/8/7/6/5/4/3/2/1"},
		},
	},
	{
		name: "no tags present",
		body: ioutil.NopCloser(strings.NewReader("<html><body></body></<html>")),
		url:  base,
		out: &Page{
			URL: base,
		},
	},
}

func TestValidateResponse(t *testing.T) {
	for _, tt := range responseCodes {
		t.Run(tt.name, func(t *testing.T) {
			err := validateResponse(tt.in)
			if err == nil && tt.err {
				t.Errorf("expecting an error for combination %q + %v", tt.in.StatusCode, tt.in.Header)
			}

			if err != nil && !tt.err {
				t.Errorf("expecting no error for combination %q + %v", tt.in.StatusCode, tt.in.Header)
			}
		})
	}
}

func TestProcessLink(t *testing.T) {
	for _, tt := range linksToProcess {
		t.Run(tt.name, func(t *testing.T) {
			c := &Crawler{baseURL: base}
			res, err := c.processLink(tt.in)
			if res != tt.out && err != nil {
				t.Errorf("not expecting an error for: %s and %s", res, err)
			}
		})
	}
}

func TestProcessBrokenLink(t *testing.T) {
	t.Run("Broken base URL", func(t *testing.T) {
		c := &Crawler{baseURL: ":foo"}
		res, err := c.processLink("/home")
		if err != nil && res != "" {
			t.Errorf("expecting an error for combination %q + %v", 1, 1)
		}
	})
}

func TestFetchLinks(t *testing.T) {
	for _, tt := range fetched {
		t.Run(tt.name, func(t *testing.T) {
			c := &Crawler{}
			p := c.fetchLinks(tt.body, tt.url)
			if !reflect.DeepEqual(tt.out, p) {
				t.Errorf("links not matching, actually: %v, expected %v", p, tt.out)
			}
		})
	}
}

func TestGetDocumentFailure(t *testing.T) {
	html := "<!DOCTYPE html><html></html>"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		fmt.Fprintln(w, html)
	}))
	defer ts.Close()

	c := &Crawler{client: http.DefaultClient}
	_, err := c.getDocument("")
	if err == nil {
		t.Errorf("error was expected")
	}
}

func TestIntegrationTest(t *testing.T) {
	u := "http://jackhugh.es"
	c := New(u)
	c.crawl(u)

	if len(c.Pages) <= 0 {
		t.Error("length of pages was 0")
	}

	for _, e := range c.Pages {
		if e.URL == "http://jackhugh.es" {
			for _, l := range e.Links {
				if len(e.Links) != 4 {
					t.Error("length of links was 0, expecting 4")
				}
				switch l {
				case "http://jackhugh.es/index.html":
					continue
				case "http://jackhugh.es/about.html":
					continue
				case "http://jackhugh.es/shop.html":
					continue
				case "http://jackhugh.es/social.html":
					continue
				default:
					t.Error("link was not as expected")
				}
			}
		}
	}
}

func BenchmarkCrawl(b *testing.B) {
	u := "http://jackhugh.es"

	for n := 0; n < b.N; n++ {
		c := New(u)
		c.crawl(u)
	}
}
