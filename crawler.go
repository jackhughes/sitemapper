package main

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/net/html"
)

// Crawler has various properties to enable the recursive crawling of web pages
// -- baseURL is the base URL the crawler will start at, e.g. https://monzo.com
// -- visited is a map[string]struct{} the string being the url and the struct indicating a failure
// -- Pages is the container for multiple URLs and their corresponding links on the page
// -- client is the http client to make outbound requests
// -- vl is the visited mutex
// -- pl is the pages mutex
type Crawler struct {
	baseURL string
	visited map[string]struct{}
	Pages   []*Page `json:"pages"`
	client  *http.Client
	vl      sync.Mutex
	pl      sync.Mutex
}

// Page is the container for URLs and the links within that page.
// -- URL is the string of the URL of the page
// -- Links is a slice of strings containing all links with the same domain on the page
type Page struct {
	URL   string   `json:"url"`
	Links []string `json:"links"`
}

// New constructor for the crawler, initialises a http client with a 15 second timeout.
func New(url string) *Crawler {
	clnt := &http.Client{
		Timeout: 15 * time.Second,
	}

	return &Crawler{
		baseURL: url,
		visited: make(map[string]struct{}),
		client:  clnt,
	}
}

// Crawl recursively crawls a URL to find links to pages, and the links on those pages.
// On failing cases the visited map is populated with the URL of the page and an empty struct,
// we could use an error here, but struct gives a slight performance increase.
// The function is recursive and does not consider depth at this point in time.
func (c *Crawler) crawl(u string) {
	var fail struct{}
	c.vl.Lock()
	if _, ok := c.visited[u]; ok {
		c.vl.Unlock()
		logrus.Infof("already fetched: %s", u)

		return
	}

	_, err := url.ParseRequestURI(u)
	if err != nil {
		logrus.Errorf("please use a fully valid URL, such as https://google.com: %v", err)
		c.vl.Unlock()
		return
	}

	// Mark the URL as loading to avoid other routines attempting it.
	c.visited[u] = fail
	logrus.Infof("processing url: %s", u)
	c.vl.Unlock()

	res, err := c.getDocument(u)
	if err != nil {
		c.vl.Lock()
		c.visited[u] = fail
		c.vl.Unlock()
		return
	}

	if err := validateResponse(res); err != nil {
		c.vl.Lock()
		c.visited[u] = fail
		c.vl.Unlock()
		return
	}

	// Pluck the links out of the page
	lnks := c.fetchLinks(res.Body, u)
	if err != nil {
		c.vl.Lock()
		c.visited[u] = fail
		c.vl.Unlock()
	}

	c.pl.Lock()
	c.Pages = append(c.Pages, lnks)
	c.pl.Unlock()

	var wg sync.WaitGroup
	// Iterate over new links in a new go routine
	for _, url := range lnks.Links {
		wg.Add(1)

		go func(url string) {
			c.crawl(url)
			wg.Done()
		}(url)
	}

	wg.Wait()
}

// Build a request to retrieve the HTML body from a URL
func (c *Crawler) getDocument(u string) (*http.Response, error) {
	req, err := http.NewRequest(http.MethodGet, u, nil)
	if err != nil {
		logrus.Errorf("could not build request: %v", err)
		return nil, err
	}

	req.Header.Set("User-Agent", "JackHughesScraper")

	res, err := c.client.Do(req)
	if err != nil {
		logrus.Errorf("could not make request: %v", err)
		return nil, err
	}

	return res, nil
}

// Parse links from the HTML body, in this case only links with a prefixed / will be considered as a valid link
func (c *Crawler) fetchLinks(body io.ReadCloser, u string) *Page {
	defer body.Close()
	p := &Page{}
	p.URL = u

	tkzr := html.NewTokenizer(body)

	for {
		tt := tkzr.Next()
		switch {
		case tt == html.ErrorToken:
			return p
		case tt == html.StartTagToken:
			t := tkzr.Token()

			anc := t.Data == "a"
			if anc {
				for _, a := range t.Attr {
					if a.Key == "href" && strings.HasPrefix(a.Val, "/") {
						l, err := c.processLink(a.Val)
						if err != nil {
							break
						}
						p.Links = append(p.Links, l)
						break
					}
				}
			}
		}
	}
}

// Process links that begin with a "/"
func (c *Crawler) processLink(link string) (string, error) {
	// Attach the BaseURL to form fully qualified links
	u, err := url.Parse(c.baseURL)
	if err != nil {
		err := fmt.Errorf("cannot parse base url: %v", c.baseURL)
		logrus.Error(err)

		return "", err
	}

	u.Path = path.Join(u.Path, link)

	return u.String(), err
}

// Validate that the response is a) 200 ok b) valid html
func validateResponse(res *http.Response) error {
	if res.StatusCode != http.StatusOK {
		err := fmt.Errorf("non-successful response code: %v", res.StatusCode)
		logrus.Error(err)

		return err
	}

	if !strings.Contains(res.Header.Get("Content-Type"), "text/html") {
		err := fmt.Errorf("not a html document, skipping")
		logrus.Error(err)

		return err
	}

	return nil
}
